#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 8080

struct User {
    char name[10000];
    char password[10000];
};

struct AllowedDatabase {
    char database[10000];
    char name[10000];
};

struct Table {
    int columnCount;
    char type[100][10000];
    char data[100][10000];
};

void createUser(char *name, char *password) {
    struct User user;
	strcpy(user.name, name);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	FILE *fp;
	fp = fopen("databases/user.txt", "ab");
	fwrite(&user,sizeof(user), 1, fp);
	fclose(fp);
}


int isUserExist(char *username) {
    FILE *fp;
    struct User user;
    int found = 0;
    fp = fopen("../database/databases/user.txt", "rb");
    while (1) {
        fread(&user, sizeof(user), 1, fp);
        if (strcmp(user.name, username) == 0) {
            return 1;
        }
        if (feof(fp)) {
            break;
        }
    }
    fclose(fp);
    return 0;
}

void insertPermission(char *name, char *database) {
    struct AllowedDatabase user;
    strcpy(user.name, name);
    strcpy(user.database, database);
    char filename[] = "databases/permission.txt";
    FILE *fp;
    fp = fopen(filename, "ab");
    fwrite(&user, sizeof(user), 1, fp);
    fclose(fp);
}

int isDatabaseAllowed(char *name, char *database) {
    FILE *fp;
    struct AllowedDatabase user;
    int found = 0;
    fp = fopen("../database/databases/permission.txt", "rb");
    while (1) {
        fread(&user, sizeof(user), 1, fp);
        if (strcmp(user.name, name) == 0) {
            if (strcmp(user.database, database) == 0) {
                return 1;
            }
        }
        if (feof(fp)) {
            break;
        }
    }
    fclose(fp);
    return 0;
}

int findColumn(char *table, char *column) {
    FILE *fp;
    struct Table user;
    int found = 0;
    fp = fopen(table, "rb");
    fread(&user, sizeof(user), 1, fp);
    int index = -1;
    for (int i = 0; i < user.columnCount; i++) {
        if (strcmp(user.data[i], column) == 0) {
            index = i;
        }
    }
    if (feof(fp)) {
        return -1;
    }
    fclose(fp);
    return index;
}

int deleteColumn(char *table, int index) {
    FILE *fp, *fp1;
    struct Table user;
    int found = 0;
    fp = fopen(table, "rb");
    fp1 = fopen("temp", "wb");
    while (1) {
        fread(&user, sizeof(user), 1, fp);
        if (feof(fp)) {
            break;
        }
        struct Table userCopy;
        int iteration = 0;
        for (int i = 0; i < user.columnCount; i++) {
            if (i == index) {
                continue;
            }
            strcpy(userCopy.data[iteration], user.data[i]);
            strcpy(userCopy.type[iteration], user.type[i]);
            iteration++;
        }
        userCopy.columnCount = user.columnCount - 1;
        fwrite(&userCopy, sizeof(userCopy), 1, fp1);
    }
    fclose(fp);
    fclose(fp1);
    remove(table);
    rename("temp", table);
    return 0;
}

int deleteTable(char *table, char *tableName) {
    FILE *fp, *fp1;
    struct Table user;
    int found = 0;
    fp = fopen(table, "rb");
    fp1 = fopen("temp", "ab");
    fread(&user, sizeof(user), 1, fp);
    struct Table userCopy;
    for (int i = 0; i < user.columnCount; i++) {
        strcpy(userCopy.data[i], user.data[i]);
        strcpy(userCopy.type[i], user.type[i]);
    }
    userCopy.columnCount = user.columnCount;
    fwrite(&userCopy, sizeof(userCopy), 1, fp1);
    fclose(fp);
    fclose(fp1);
    remove(table);
    rename("temp", table);
    return 1;
}

int updateColumn(char *table, int index, char *replacement) {
    FILE *fp, *fp1;
    struct Table user;
    int found = 0;
    fp = fopen(table, "rb");
    fp1 = fopen("temp", "ab");
    int dataCount = 0;
    while (1) {
        fread(&user, sizeof(user), 1, fp);
        if (feof(fp)) {
            break;
        }
        struct Table userCopy;
        int iteration = 0;
        for (int i = 0; i < user.columnCount; i++) {
            if (i == index && dataCount != 0) {
                strcpy(userCopy.data[iteration], replacement);
            }
            else {
                strcpy(userCopy.data[iteration], user.data[i]);
            }
            strcpy(userCopy.type[iteration], user.type[i]);
            iteration++;
        }
        userCopy.columnCount = user.columnCount;
        fwrite(&userCopy, sizeof(userCopy), 1, fp1);
        dataCount++;
    }
    fclose(fp);
    fclose(fp1);
    remove(table);
    rename("temp", table);
    return 0;
}

int updateColumnWhere(char *table, int index, char *replacement, int replaceIndex, char *where) {
    FILE *fp, *fp1;
    struct Table user;
    int found = 0;
    fp = fopen(table, "rb");
    fp1 = fopen("temp", "ab");
    int dataCount = 0;
    while (1) {
        fread(&user, sizeof(user), 1, fp);
        if (feof(fp)) {
            break;
        }
        struct Table userCopy;
        int iteration = 0;
        for (int i = 0; i < user.columnCount; i++) {
            if (i == index && dataCount != 0 && strcmp(user.data[replaceIndex], where) == 0) {
                strcpy(userCopy.data[iteration], replacement);
            }
            else {
                strcpy(userCopy.data[iteration], user.data[i]);
            }
            strcpy(userCopy.type[iteration], user.type[i]);
            iteration++;
        }
        userCopy.columnCount = user.columnCount;
        fwrite(&userCopy, sizeof(userCopy), 1, fp1);
        dataCount++;
    }
    fclose(fp);
    fclose(fp1);
    remove(table);
    rename("temp", table);
    return 0;
}

int deleteTableWhere(char *table, int index, char *columnName, char *where) {
    FILE *fp, *fp1;
    struct Table user;
    int found = 0;
    fp = fopen(table, "rb");
    fp1 = fopen("temp", "ab");
    int dataCount = 0;
    while (1) {
        found = 0;
        fread(&user, sizeof(user), 1, fp);
        if (feof(fp)) {
            break;
        }
        struct Table userCopy;
        int iteration = 0;
        for (int i = 0; i < user.columnCount; i++) {
            if (i == index && dataCount != 0 && strcmp(user.data[i], where) == 0) {
                found = 1;
            }
            strcpy(userCopy.data[iteration], user.data[i]);
            strcpy(userCopy.type[iteration], user.type[i]);
            iteration++;
        }
        userCopy.columnCount = user.columnCount;
        if (found != 1) {
            fwrite(&userCopy, sizeof(userCopy), 1, fp1);
        }
        dataCount++;
    }
    fclose(fp);
    fclose(fp1);
    remove(table);
    rename("temp", table);
    return 0;
}

void writeLog(char *command, char *username) {
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    char logInfo[1000];

    FILE *file;
    file = fopen("log.txt", "ab");

    sprintf(logInfo, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, username, command);

    fputs(logInfo, file);
    fclose(file);
    return;
}

int main(){

	int sockfd, ret;
	 struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		printf("Error in connection.\n");
		exit(1);
	}
	printf("Server Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("Error in binding.\n");
		exit(1);
	}
	printf("Bind to port %d\n", 8080);

	if(listen(sockfd, 10) == 0){
		printf("Listening....\n");
	}else{
		printf("Error in binding.\n");
	}


	while(1){
		newSocket = accept(sockfd, (struct sockaddr*)&newAddr, &addr_size);
		if(newSocket < 0){
			exit(1);
		}
		printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

		if((childpid = fork()) == 0){
			close(sockfd);

			while(1){
				recv(newSocket, buffer, 1024, 0);
				char *token;
				char buffercopy[32000];
				strcpy(buffercopy, buffer);
				char command[100][10000];
				token = strtok(buffercopy, ":");
				int i=0;
				char database_used[1000];
				while( token != NULL ) {
					strcpy(command[i], token);
					i++;
					token = strtok(NULL, ":");
				}
				if(strcmp(command[0], "CreateUser")==0){
					if(strcmp(command[3], "0")==0){
						createUser(command[1], command[2]);
					}else{
						char message[] = "You're Not Allowed";
						send(newSocket, message, strlen(message), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(command[0], "GrantPermission")==0){
					if(strcmp(command[3], "0")==0){
						int exist = isUserExist(command[2]);
						if(exist == 1){
							insertPermission(command[2], command[1]);
						}else{
							char message[] = "User Not Found";
							send(newSocket, message, strlen(message), 0);
							bzero(buffer, sizeof(buffer));
						}
					}else{
						char message[] = "You're Not Allowed";
						send(newSocket, message, strlen(message), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(command[0], "CreateDatabase")==0){
					char lokasi[20000];
					snprintf(lokasi, sizeof lokasi, "databases/%s", command[1]);
					mkdir(lokasi,0777);
					insertPermission(command[2], command[1]);
				}else if(strcmp(command[0], "UseDatabase") == 0){
					if(strcmp(command[3], "0") != 0){
						int allowed = isDatabaseAllowed(command[2], command[1]);
						if(allowed != 1){
							char message[] = "Access_database : You're Not Allowed";
							send(newSocket, message, strlen(message), 0);
							bzero(buffer, sizeof(buffer));
						}else{
							strncpy(database_used, command[1], sizeof(command[1]));
							char message[] = "Access_database : Allowed";
							send(newSocket, message, strlen(message), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
				}else if(strcmp(command[0], "cekCurrentDatabase")==0){
					if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
					}
					send(newSocket, database_used, strlen(database_used), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(command[0], "CreateTable")==0){
					char *tokens;
					if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
					}else{
                        char Query[100][10000];
                        char copyCommand[20000];
                        snprintf(copyCommand, sizeof copyCommand, "%s", command[1]);
                        tokens = strtok(copyCommand, "(), ");
                        int count=0;
                        while( tokens != NULL ) {
                            strcpy(Query[count], tokens);
                            count++;
                            tokens = strtok(NULL, "(), ");
                        }
                        char createTable[20000];
                        snprintf(createTable, sizeof createTable, "../database/databases/%s/%s", database_used, Query[2]);
                        int iteration = 0;
                        int iterationData = 3;
                        struct Table column;
                        while(count > 3){
                            strcpy(column.data[iteration], Query[iterationData]);
                            strcpy(column.type[iteration], Query[iterationData+1]);
                            iterationData = iterationData+2;
                            count=count-2;
                            iteration++;
                        }
                        column.columnCount = iteration;
                        FILE *fp;
                        fp=fopen(createTable,"ab");
                        fwrite(&column,sizeof(column),1,fp);
                        fclose(fp);
                    }
				}else if(strcmp(command[0], "DropDatabase")==0){
					int allowed = isDatabaseAllowed(command[2], command[1]);
					if(allowed != 1){
						char message[] = "Access_database : You're Not Allowed";
						send(newSocket, message, strlen(message), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}else{
						char delete[20000];
						snprintf(delete, sizeof delete, "rm -r databases/%s", command[1]);
						system(delete);
						char message[] = "Database Has Been Removed";
						send(newSocket, message, strlen(message), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(command[0], "DropTable")==0){
					if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char delete[20000];
					snprintf(delete, sizeof delete, "databases/%s/%s", database_used ,command[1]);
					remove(delete);
					char message[] = "Table Has Been Removed";
					send(newSocket, message, strlen(message), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(command[0], "DropColumn")==0){
					if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char createTable[20000];
					snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, command[2]);
					int index = findColumn(createTable, command[1]);
                    if(index == -1){
                        char message[] = "Column Not Found";
                        send(newSocket, message, strlen(message), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
					deleteColumn(createTable, index);
					char message[] = "Column Has Been Removed";
					send(newSocket, message, strlen(message), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(command[0], "Insert")==0){
                    if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char Query[100][10000];
					char copyCommand[20000];
					snprintf(copyCommand, sizeof copyCommand, "%s", command[1]);
                    char *tokens;
                    tokens = strtok(copyCommand, "\'(), ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(Query[count], tokens);
						count++;
						tokens = strtok(NULL, "\'(), ");
					}
                    char createTable[20000];
					snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, Query[2]);
                    FILE *fp;
                    int banyakKolom;
					fp=fopen(createTable,"rb");
                    if (fp == NULL){
                        char message[] = "TABLE NOT FOUND";
                        send(newSocket, message, strlen(message), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }else{
                        struct Table user;
                        fread(&user,sizeof(user),1,fp);
                        banyakKolom=user.columnCount;
                        fclose(fp);
                    }
					int iteration = 0;
					int iterationData = 3;
					struct Table column;
					while(count > 3){
						strcpy(column.data[iteration], Query[iterationData]);
						strcpy(column.type[iteration], "string");
						iterationData++;
						count=count-1;
						iteration++;
					}
					column.columnCount = iteration;
                    if(banyakKolom != column.columnCount){
                        char message[] = "YOUR INPUT NOT MATCH THE COLUMN";
                        send(newSocket, message, strlen(message), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }   
                    FILE *fp1;
                    fp1=fopen(createTable,"ab");
                    fwrite(&column,sizeof(column),1,fp1);
                    fclose(fp1);
                    char message[] = "Data Has Been Inserted";
					send(newSocket, message, strlen(message), 0);
					bzero(buffer, sizeof(buffer));
                }else if(strcmp(command[0], "Update")==0){
                    if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char Query[100][10000];
					char copyCommand[20000];
					snprintf(copyCommand, sizeof copyCommand, "%s", command[1]);
                    char *tokens;
                    tokens = strtok(copyCommand, "\'(),= ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(Query[count], tokens);
						count++;
						tokens = strtok(NULL, "\'(),= ");
					}
                    char createTable[20000];
					snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, Query[1]);
                    if(count==5){
                        int index = findColumn(createTable, Query[3]);
                        if(index == -1){
                            char message[] = "Column Not Found";
                            send(newSocket, message, strlen(message), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        updateColumn(createTable, index, Query[4]);
                    }else if(count==8){
                        int index = findColumn(createTable, Query[3]);
                        if(index == -1){
                            char message[] = "Column Not Found";
                            send(newSocket, message, strlen(message), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        int changeIndex = findColumn(createTable, Query[6]);
                        updateColumnWhere(createTable, index, Query[4], changeIndex ,Query[7]);
                    }else{
                        char message[] = "Data Has Been Deleted";
                        send(newSocket, message, strlen(message), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    char message[] = "Data Has Been Updated";
					send(newSocket, message, strlen(message), 0);
					bzero(buffer, sizeof(buffer));

                }else if(strcmp(command[0], "Delete")==0){
                    if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char Query[100][10000];
					char copyCommand[20000];
					snprintf(copyCommand, sizeof copyCommand, "%s", command[1]);
                    char *tokens;
                    tokens = strtok(copyCommand, "\'(),= ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(Query[count], tokens);
						count++;
						tokens = strtok(NULL, "\'(),= ");
					}
                    char createTable[20000];
					snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, Query[2]);
                    if(count==3){
                        deleteTable(createTable, Query[2]);
                    }else if(count==6){
                        int index = findColumn(createTable, Query[4]);
                        if(index == -1){
                            char message[] = "Column Not Found";
                            send(newSocket, message, strlen(message), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        deleteTableWhere(createTable, index, Query[4], Query[5]);
                    }else{
                        char message[] = "Input Salah";
                        send(newSocket, message, strlen(message), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    char message[] = "Data Has Been Deleted";
					send(newSocket, message, strlen(message), 0);
					bzero(buffer, sizeof(buffer));
                }else if(strcmp(command[0], "Select")==0){
                    if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char Query[100][10000];
					char copyCommand[20000];
					snprintf(copyCommand, sizeof copyCommand, "%s", command[1]);
                    char *tokens;
                    tokens = strtok(copyCommand, "\'(),= ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(Query[count], tokens);
						count++;
						tokens = strtok(NULL, "\'(),= ");
					}
                    if(count == 4){
						char createTable[20000];
						snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, Query[3]);
                        if(strcmp(Query[1], "*")==0){
                            FILE *fp, *fp1;
                            struct Table user;
                            int id,found=0;
                            fp=fopen(createTable,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1){	
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.columnCount; i++){
                                    char padding[20000];
                                    snprintf(padding, sizeof padding, "%s\t",user.data[i]);
                                    strcat(buffers, padding);
                                }
                                strcat(sendDatabase, buffers);
                            }
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer));
                            fclose(fp);
                        }else{
                            int index = findColumn(createTable, Query[1]);
                            FILE *fp, *fp1;
                            struct Table user;
                            int id,found=0;
                            fp=fopen(createTable,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1){	
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.columnCount; i++){
                                    if(i == index){
                                        char padding[20000];
                                        snprintf(padding, sizeof padding, "%s\t",user.data[i]);
                                        strcat(buffers, padding);
                                    }
                                }
                                strcat(sendDatabase, buffers);
                            }
                            fclose(fp);
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer));
                        }
                    }else if(count == 7 && strcmp(Query[4], "WHERE")==0){
						char createTable[20000];
						snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, Query[3]);
                        if(strcmp(Query[1], "*")==0){
                            FILE *fp, *fp1;
                            struct Table user;
                            int id,found=0;
                            fp=fopen(createTable,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            int index = findColumn(createTable, Query[5]);
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1){	
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.columnCount; i++){
                                    if(strcmp(user.data[index], Query[6])==0){
                                        char padding[20000];
                                        snprintf(padding, sizeof padding, "%s\t",user.data[i]);
                                        strcat(buffers, padding);
                                    }
                                }
                                strcat(sendDatabase, buffers);
                            }
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer));
                            fclose(fp);
                        }else{
                            int index = findColumn(createTable, Query[1]);
                            FILE *fp, *fp1;
                            struct Table user;
                            int id,found=0;
                            int changeIndex = findColumn(createTable, Query[5]);
                            fp=fopen(createTable,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1){	
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.columnCount; i++){
                                    if(i == index && (strcmp(user.data[changeIndex], Query[6])==0 || strcmp(user.data[i],Query[5])==0)){
                                        char padding[20000];
                                        snprintf(padding, sizeof padding, "%s\t",user.data[i]);
                                        strcat(buffers, padding);
                                    }
                                }
                                strcat(sendDatabase, buffers);
                            }
                            fclose(fp);
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer));
                        }
                    }else{
						if(strcmp(Query[count-3], "WHERE")!= 0){
							char createTable[20000];
							snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, Query[count-1]);
							int index[100];
							int iteration=0;
							for(int i=1; i<count-2; i++){
								index[iteration] = findColumn(createTable, Query[i]);
								iteration++;
							}
						}else if(strcmp(Query[count-3], "WHERE")== 0){
							printf("dengan where");
						}
					}
                }else if(strcmp(command[0], "log")==0){
					writeLog(command[1], command[2]);
					char message[] = "\n";
					send(newSocket, message, strlen(message), 0);
					bzero(buffer, sizeof(buffer));
				}
				if(strcmp(buffer, "exit") == 0){
					printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				}else{
					printf("Client: %s\n", buffer);
					send(newSocket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}

	}

	close(newSocket);
	return 0;
}
