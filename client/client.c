#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

struct allowed {
    char name[100];
    char password[100];
};

int checkAllowed(char *username, char *password) {
    FILE *fp;
    struct allowed user;
    int found = 0;
    fp = fopen("../database/databases/user.txt", "rb");
    if (fp == NULL) {
        printf("Error opening file.\n");
        return 0;
    }
    while (fread(&user, sizeof(user), 1, fp)) {
        if (strcmp(user.name, username) == 0){
            if (strcmp(user.password, password) == 0) {
                found = 1;
        }
        if(feof(fp))
            break;
    }
    
    fclose(fp);
    if (found == 0) {
        printf("You're Not Allowed\n");
        return 0;
    } else {
        return 1;
    }
}

void writeLog(char *command, char *name) {
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    char infoWriteLog[1000];

    FILE *file;
    char location[100];
    snprintf(location, sizeof(location), "../database/log/log.txt");
    file = fopen(location, "ab");

    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1,
            timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, name, command);

    fputs(infoWriteLog, file);
    fclose(file);
    return;
}

int main(int argc, char *argv[]) {
    int allowed = 0;
    int id_user = geteuid();
    char database_used[1000];
    if (geteuid() == 0) {
        allowed = 1;
    } else {
        allowed = checkAllowed(argv[2], argv[4]);
    }
    if (allowed == 0) {
        return 0;
    }

    int clientSocket, ret;
    struct sockaddr_in serverAddr;
    char buffer[32000];

    clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clientSocket < 0) {
        printf("Error in connection.\n");
        exit(1);
    }
    printf("Client Socket is created.\n");

    memset(&serverAddr, '\0', sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
    if (ret < 0) {
        printf("Error in connection.\n");
        exit(1);
    }
    printf("Connected to Server.\n");

    while (1) {
        printf("Client: \t");
        char input[1000];
        char copyInput[1000];
        char commands[100][1000];
        char *token;
        int i = 0;
        scanf(" %[^\n]s", input);
        strcpy(copyInput, input);
        token = strtok(input, " ");
        while (token != NULL) {
            strcpy(commands[i], token);
            i++;
            token = strtok(NULL, " ");
        }

        int wrongCommand = 0;
        if (strcmp(commands[0], "CREATE") == 0) {
            if (strcmp(commands[1], "USER") == 0 && strcmp(commands[3], "IDENTIFIED") == 0 && strcmp(commands[4], "BY") == 0) {
                snprintf(buffer, sizeof(buffer), "CreateUser:%s:%s:%d", commands[2], commands[5], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(commands[1], "DATABASE") == 0) {
                snprintf(buffer, sizeof(buffer), "CreateDatabase:%s:%s:%d", commands[2], argv[2], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(commands[1], "TABLE") == 0) {
                snprintf(buffer, sizeof(buffer), "CreateTable:%s", copyInput);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        } else if (strcmp(commands[0], "GRANT") == 0 && strcmp(commands[1], "PERMISSION") == 0 && strcmp(commands[3], "INTO") == 0) {
            snprintf(buffer, sizeof(buffer), "GrantPermission:%s:%s:%d", commands[2], commands[4], id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(commands[0], "USE") == 0) {
            snprintf(buffer, sizeof(buffer), "UseDatabase:%s:%s:%d", commands[1], argv[2], id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(commands[0], "cekCurrentDatabase") == 0) {
            snprintf(buffer, sizeof(buffer), "%s", commands[0]);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(commands[0], "DROP") == 0) {
            if (strcmp(commands[1], "DATABASE") == 0) {
                snprintf(buffer, sizeof(buffer), "DropDatabase:%s:%s", commands[2], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(commands[1], "TABLE") == 0) {
                snprintf(buffer, sizeof(buffer), "DropTable:%s:%s", commands[2], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(commands[1], "COLUMN") == 0) {
                snprintf(buffer, sizeof(buffer), "DropColumn:%s:%s:%s", commands[2], commands[4], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        } else if (strcmp(commands[0], "INSERT") == 0 && strcmp(commands[1], "INTO") == 0) {
            snprintf(buffer, sizeof(buffer), "Insert:%s", copyInput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(commands[0], "UPDATE") == 0) {
            snprintf(buffer, sizeof(buffer), "Update:%s", copyInput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(commands[0], "DELETE") == 0 && strcmp(commands[1], "FROM") == 0) {
            snprintf(buffer, sizeof(buffer), "Delete:%s", copyInput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(commands[0], "SELECT") == 0) {
            snprintf(buffer, sizeof(buffer), "Select:%s", copyInput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(commands[0], "exit") == 0) {
            snprintf(buffer, sizeof(buffer), "Exit:%d", id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
            break;
        } else {
            wrongCommand = 1;
            printf("Command not recognized.\n");
        }

        if (wrongCommand == 0) {
            writeLog(copyInput, argv[2]);
            memset(buffer, '\0', sizeof(buffer));
            recv(clientSocket, buffer, 32000, 0);
            printf("Server: \t%s\n", buffer);
        }

        memset(buffer, '\0', sizeof(buffer));
    }

    return 0;
}
